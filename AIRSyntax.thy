theory AIRSyntax
  imports Main DualMap
begin

section \<open>AIR Syntax\<close>

subsection \<open>Expressions\<close>

text \<open>All values and addresses are evaluated to a constant type (<const>) which in this model is an
integer.\<close>

type_synonym const = int

text \<open>True and False represented in AIR constants.\<close>

definition "air_true = 1"

text \<open>Registers are modelled by a <reg> type. V (int) refers to a virtual register - sometimes known
      as a temporary register.\<close>

datatype reg = RAX | RBX | RCX | RDX | RDI | CF | V int

text \<open>Operations are modelled as functions, where unary operations evaluate the result of the a 
function on a single constant and binary operations evaluate the result of a function on two 
constants.\<close>

type_synonym un_op = "const \<Rightarrow> const"
type_synonym bin_op = "const \<Rightarrow> const \<Rightarrow> const"

text \<open>An expression in AIR can be one of four types:

Const - evaluates to the specified constant
Reg   - read the constant in the specified register
UnOp  - evaluate expression and apply unary operation to the constant result
BinOp - evaluate both expressions and apply binary operation to the constant results

Evaluation of these expressions is shown the Operational Semantics
\<close>

datatype exp =
    Const const
  | Reg reg 
  | UnOp un_op exp
  | BinOp exp bin_op exp


subsection \<open>Instructions\<close>


text \<open>The instructions are standard AIR syntax with the addition of SpecFence.

RegisterUpdate - Update the given register
Load           - Load the value from memory into the given register
Store          - Store the value in memory
Branch         - If the expression evaluates to true (1) then go to the program address
Goto           - Go to the program address
SpecFence      - Force the processor to resolve outstanding branches
Havoc          - Special instruction. Adversary reduction lemma, see Adversary Model.

Evaluation of these instructions is shown the Operational Semantics
.\<close>

datatype instr = 
    RegisterUpdate reg exp
  | Load reg exp
  | Store exp exp
  | Branch exp const
  | Goto const
  | SpecFence

subsection \<open>Machine State\<close>

text \<open>Currently addresses are constant, this is a limitation highlighted in the paper. HALT is a 
program address indicating that the end of the program has been reached \<close>

type_synonym Address = const

definition "HALT = -1"

text \<open>The speculation level is a natural number representing the level of speculation\<close>

type_synonym SpeculationLevel = nat

text \<open>The register and data memory states are maps of the speculation level and address/register to 
a constant value. This models the state of registers at different speculation 
levels\<close>

type_synonym RegisterState = "(SpeculationLevel, reg, const) dual_map"
type_synonym DataMemoryState = "(SpeculationLevel, int, const) dual_map"

text \<open>The program counter indexed by speculation level\<close>

type_synonym ProgramCounter = "SpeculationLevel \<rightharpoonup> Address"

text \<open>Branch predictor state, a trace of the speculation level, addresses and executed instruction.\<close>

type_synonym BranchPredictorState = "(SpeculationLevel * Address) list"

text \<open>Program Memory, a map of addresses to program instructions.\<close>

type_synonym ProgramMemory = "Address \<rightharpoonup> instr"

text \<open>Trace of accessed program addresses and data addresses (which are optional).\<close>

type_synonym AddressTrace = "(Address \<times> Address option) list"


text \<open>The machine state\<close>

record MachineState =
  \<Pi> :: ProgramMemory
  \<Delta> :: RegisterState
  \<mu> :: DataMemoryState
  pc :: ProgramCounter
  \<omega> :: AddressTrace
  \<beta> :: BranchPredictorState
  n :: SpeculationLevel

text \<open>Retrieve the program address for the current state\<close>

definition "\<rho> s \<equiv> (the (pc s (n s)))"

text \<open>Retrieve the instruction for the current state\<close>

definition "\<iota> s \<equiv> the (\<Pi> s (\<rho> s))"

text \<open>Predicate indicating that the state is currently halting (end of program)\<close>

definition "halting s \<equiv> \<rho> s = HALT"

text \<open>Predicate indicating that the state is currently speculating\<close>

definition "speculating s \<equiv> n s > 0"

subsection \<open>Speculative branch resolution\<close>

text \<open>A branch can be resolved at any point (non-deterministic) given that we are currently 
      speculating (n > 0). If we are currently halting (end of program) then the only action we can 
      take is to resolve until n = 0.\<close>

definition "resolve p\<^sub>n p\<^sub>\<beta> p\<^sub>p\<^sub>c = (p\<^sub>n > (0::SpeculationLevel) 
  \<and> (p\<^sub>p\<^sub>c p\<^sub>n = Some HALT \<or> (\<some> x. x \<in> {True, False})))"

lemma [simp]: "\<not> resolve 0 p\<^sub>\<beta> p\<^sub>p\<^sub>c"
  by (simp add: resolve_def)

lemma [simp]: "\<lbrakk>p\<^sub>n > 0; p\<^sub>p\<^sub>c p\<^sub>n = Some HALT\<rbrakk> \<Longrightarrow> resolve p\<^sub>n p\<^sub>\<beta> p\<^sub>p\<^sub>c"
  by (simp add: resolve_def)

lemma [simp]: "\<lbrakk>p\<^sub>n > 0; p\<^sub>p\<^sub>c p\<^sub>n \<noteq> Some HALT\<rbrakk> \<Longrightarrow> (resolve p\<^sub>n p\<^sub>\<beta> p\<^sub>p\<^sub>c) \<in> {True, False}"
  by (simp add: resolve_def)


subsection \<open>Speculative branch miss-predictions\<close>

text \<open>The branch predictor may mispredict the outcome of a branch expression, triggering 
      speculative execution. This is non-deterministic, making no assumptions as to the state of the 
      branch predictor and it's likely prediction.\<close>

definition "mispred p\<^sub>n p\<^sub>\<beta> p\<^sub>p\<^sub>c = (\<some> x. x \<in> {True, False})"

lemma [simp]: "(mispred p\<^sub>n p\<^sub>\<beta> \<rho>) \<in> {True, False}"
  by (simp add: mispred_def)


subsection \<open>Updating the state of the branch predictor\<close>

text \<open>The branch predictor is updated by appending the speculation level and program address. 
      A mismatch in traces between two branch predictors indicates the divergence of paths.\<close>

definition "update p\<^sub>n p\<^sub>\<rho> p\<^sub>\<beta> = p\<^sub>\<beta> @ [(p\<^sub>n, p\<^sub>\<rho>)]"


subsection \<open>Well Formedness\<close>

text \<open>A program address is well formed if it points to an instruction in the program memory or 
indicates that the program has finished with HALT.\<close>

definition "wf\<rho> p\<^sub>\<rho> p\<^sub>\<Pi> \<equiv> p\<^sub>\<rho> = HALT \<or> p\<^sub>\<rho> \<in> dom p\<^sub>\<Pi>"

text \<open>Recursive function for checking that an instruction is well formed given the program memory. 
      This checks that any jump instruction leads to a well formed program address described in wf\<rho>\<close>

fun wf\<iota>_in_\<Pi> :: "instr \<Rightarrow> ProgramMemory \<Rightarrow> bool" where
"wf\<iota>_in_\<Pi> (Branch _ p\<^sub>\<rho>) p\<^sub>\<Pi> = wf\<rho> p\<^sub>\<rho> p\<^sub>\<Pi>" |
"wf\<iota>_in_\<Pi> (Goto p\<^sub>\<rho>)     p\<^sub>\<Pi> = wf\<rho> p\<^sub>\<rho> p\<^sub>\<Pi>" |
"wf\<iota>_in_\<Pi> _            _  = True"

text \<open>Program address (\<rho>) is well formed in the program memory (\<Pi>). \<rho> must be contiguous unless the 
      instruction is a goto (in which we jump to another memory address). HALT is not a valid memory
      address as it has meaning in the program and therefore should not appear in the program 
      memory.\<close>

definition "wf\<rho>_in_\<Pi> p\<^sub>\<rho> p\<^sub>\<Pi> \<equiv> (p\<^sub>\<rho> \<noteq> HALT \<and> 
  (case the (p\<^sub>\<Pi> p\<^sub>\<rho>) of Goto c \<Rightarrow> True | i \<Rightarrow> (p\<^sub>\<rho> + 1) \<in> dom p\<^sub>\<Pi>))"

text \<open>Program memory is well formed if all instructions and memory addresses are well formed 
      according to wf\<rho>_in_\<Pi> and wf\<iota>_in_\<Pi>. The last instruction should always be a Goto HALT.\<close>

definition "wf\<Pi> s \<equiv>
  let p\<^sub>\<Pi> = \<Pi> s in 
    (\<forall>i . i \<in> ran p\<^sub>\<Pi> \<longrightarrow> wf\<iota>_in_\<Pi> i p\<^sub>\<Pi>) \<and>
    (\<forall>\<rho> . \<rho> \<in> dom p\<^sub>\<Pi> \<longrightarrow> wf\<rho>_in_\<Pi> \<rho> p\<^sub>\<Pi>) \<and>
    p\<^sub>\<Pi> (Max (dom p\<^sub>\<Pi>)) = Some (Goto HALT)
"

text \<open>Current instruction is well formed if it is well formed in the states program memory 
      (wf\<iota>_in_\<Pi>).\<close>

definition "wf\<iota> s \<equiv> \<not> halting s \<longrightarrow> wf\<iota>_in_\<Pi> (\<iota> s) (\<Pi> s)"

text \<open>The Speculation level is well formed if it is greater than 0 (implied by nat type).\<close>

definition "wfn s \<equiv> True"

text \<open>The register state is well formed if for all speculation levels it is defined.\<close>

definition "wf\<Delta> s \<equiv> (\<forall>n'. n' \<le> (n s) \<longrightarrow> \<Delta> s n' \<noteq> None)"

text \<open>The data memory is well formed if for all speculation levels it is defined.\<close>

definition "wf\<mu> s \<equiv> (\<forall>n'. n' \<le> (n s) \<longrightarrow> \<mu> s n' \<noteq> None)"

text \<open>The program counter is well formed if for all speculation levels it is defined and the address
      it points to is valid.\<close>

definition "wfpc s \<equiv> (\<forall>n'. n' \<le> (n s) \<longrightarrow> (let \<rho> = pc s n' in \<rho> \<noteq> None \<and> wf\<rho> (the \<rho>) (\<Pi> s)))"

text \<open>The program trace is well formed if it's program address is in the domain of the program 
      memory.\<close>

definition "wf\<omega> s \<equiv> (\<forall>t . t \<in> set (\<omega> s) \<longrightarrow> (fst t) \<in> dom (\<Pi> s))"

text \<open>The branch predictor is well formed if it's program address is in the domain of the program 
      memory and the instruction at that address equals the logged instruction.\<close>

definition "wf\<beta> s \<equiv> (\<forall>t . t \<in> set (\<beta> s) \<longrightarrow> wf\<rho> (snd t) (\<Pi> s))"

text \<open>The wfs(s) predicate indicates that the state s is well-formed. Well-formedness is invariant
for any program.\<close>

definition "wfs s \<equiv> 
  wf\<Pi> s \<and>
  wf\<Delta> s \<and>
  wf\<mu> s \<and>
  wfpc s \<and>
  wf\<omega> s \<and>
  wf\<beta> s \<and>
  wfn s"

end

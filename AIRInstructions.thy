theory AIRInstructions
  imports Main AIRExpressions
begin

section \<open>AIR Instructions\<close>

subsection \<open>Statement Semantics\<close>

subsubsection \<open>RegisterUpdate\<close>

text \<open>Assigns the value v to the register r adding just the program address to the trace of accessed
 addresses omega. After the update the program counter will be incremented and the next instruction 
chosen. This will only modify the program state at the current speculative level.\<close>

definition "trans_register_update s p\<^sub>\<rho> r e \<equiv>
  let v = \<Delta> s, n s \<turnstile> e in
    s\<lparr>
      \<Delta> := (\<Delta> s) [n s, r]\<^sub>m \<mapsto> v,
      pc := pc s ((n s) \<mapsto> (p\<^sub>\<rho> + 1)),
      \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)]
    \<rparr>
"


subsubsection \<open>Load\<close>

text \<open>Updates the register r with the value stored at address a adding the program and memory
addresses to the trace of accessed addresses omega. After the update the program counter will be 
incremented and the next instruction chosen. This will only modify the program state at the current
speculative level.\<close>

definition "trans_load s p\<^sub>\<rho> r e \<equiv>
  let a = \<Delta> s, n s \<turnstile> e in
    let v = the ((\<mu> s) [n s, a]\<^sub>m) in
      s\<lparr>
        \<Delta> := (\<Delta> s) [n s, r]\<^sub>m \<mapsto> v,
        pc := pc s (n s \<mapsto> p\<^sub>\<rho> + 1),
        \<omega> := \<omega> s @ [(p\<^sub>\<rho>, Some a)]
      \<rparr>
"


subsubsection \<open>Store\<close>

text \<open>Updates the memory address a with the value v adding the program and memory addresses to the 
trace of accessed addresses omega. After the update the program counter will be  incremented and the 
next instruction chosen. This will only modify the program state at the current speculative level.\<close>

definition "trans_store s p\<^sub>\<rho> e\<^sub>1 e\<^sub>2 \<equiv>
  let a = \<Delta> s, n s \<turnstile> e\<^sub>1 in 
    let v = \<Delta> s, n s \<turnstile> e\<^sub>2 in
      s\<lparr>
        \<mu> := (\<mu> s) [n s, a]\<^sub>m \<mapsto> v,
        pc := pc s (n s \<mapsto> p\<^sub>\<rho> + 1),
        \<omega> := \<omega> s @ [(p\<^sub>\<rho>, Some a)]
      \<rparr>
"


subsubsection \<open>T-Pred\<close>

text \<open>A correct prediction on a branch statement that evaluates true. The program counter
will advance to the specified program address and the branch predictor will be updated with the 
evaluation.\<close>

definition "trans_tpred s p\<^sub>\<rho> c \<equiv>
  s\<lparr>
    \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s),
    pc := pc s (n s \<mapsto> c),
    \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)]
   \<rparr>
"


subsubsection \<open>T-Mispred\<close>

text \<open>An incorrect prediction on a branch statement that evaluates true. This triggers 
miss-speculation (increments n), jumping the program counter to the address specified (c) and 
updating the branch predictor.\<close>

definition "trans_tmispred s p\<^sub>\<rho> c \<equiv> 
  let n' = Suc (n s) in 
    s\<lparr>
      \<Delta> := dual_copy (\<Delta> s) (n s) n',
      \<mu> := dual_copy (\<mu> s) (n s) n',
      \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s),
      pc := pc s((n s) \<mapsto> c, n' \<mapsto> p\<^sub>\<rho> + 1),
      n := n',
      \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)]
     \<rparr>
"


subsubsection \<open>NT-Pred\<close>

text \<open>A correct prediction on a branch statement that evaluates false. The program counter will move
to the next instruction and update the branch predictor.\<close>

definition "trans_ntpred s p\<^sub>\<rho> \<equiv> 
  s\<lparr>
    \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s),
    pc := pc s(n s \<mapsto> p\<^sub>\<rho> + 1),
    \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)]
   \<rparr>
"


subsubsection \<open>NT-Mispred\<close>

text \<open>An incorrect prediction on a branch statement that evaluates false. This triggers 
miss-speculation (increments n), increments the program counter to point to the next instruction and
updates the branch predictor.\<close>

definition "trans_ntmispred s p\<^sub>\<rho> c \<equiv> 
  let n' = Suc (n s) in 
    s\<lparr>
      \<Delta> := dual_copy (\<Delta> s) (n s) n',
      \<mu> := dual_copy (\<mu> s) (n s) n',
      \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s),
      \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)],
      n := n',
      pc := pc s (n s \<mapsto> p\<^sub>\<rho> + 1, n' \<mapsto> c)
     \<rparr>
"


subsubsection \<open>Goto\<close>

text \<open>Advances the program counter to the given instruction address, updating the branch predictor
in the process.\<close>

definition "trans_goto s p\<^sub>\<rho> c \<equiv>
  s\<lparr>
    \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s),
    pc := pc s (n s \<mapsto> c),
    \<omega> := \<omega> s @ [(p\<^sub>\<rho>, None)]
   \<rparr>
"


subsubsection \<open>SpecFence\<close>

text \<open>Immediately resolves all miss-predictions, effectively terminating speculative execution.\<close>

definition "trans_specfence s \<equiv> s\<lparr>n := 0\<rparr>"


subsubsection \<open>Resolve\<close>

text \<open>Resolves the current branch miss-prediction, decreasing the speculation level and updating the 
branch predictor.\<close>

definition "trans_resolve s p\<^sub>\<rho> \<equiv> 
  s\<lparr>
    n := (n s) - 1,
    \<beta> := update (n s) p\<^sub>\<rho> (\<beta> s)
   \<rparr>
"


subsection \<open>Branch Evaluation\<close>

fun eval_pred :: "MachineState \<Rightarrow> Address \<Rightarrow> Address \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> MachineState" where
"eval_pred s p\<^sub>\<rho> c True  True  = trans_tmispred s p\<^sub>\<rho> c" |
"eval_pred s p\<^sub>\<rho> c True  False = trans_tpred s p\<^sub>\<rho> c" |
"eval_pred s p\<^sub>\<rho> c False True  = trans_ntmispred s p\<^sub>\<rho> c" |
"eval_pred s p\<^sub>\<rho> _ False False = trans_ntpred s p\<^sub>\<rho>"

lemma eval_pred_cases: 
"(s' = eval_pred s p\<^sub>\<rho> c eval pred)
  \<Longrightarrow> \<lbrakk>s' = trans_tmispred s p\<^sub>\<rho> c \<and> eval \<and> pred
        \<Longrightarrow> P s (trans_tmispred s p\<^sub>\<rho> c)\<rbrakk>
  \<Longrightarrow> \<lbrakk>s' = trans_tpred s p\<^sub>\<rho> c \<and> eval \<and> \<not>pred
        \<Longrightarrow> P s (trans_tpred s p\<^sub>\<rho> c)\<rbrakk>
  \<Longrightarrow> \<lbrakk>s' = trans_ntmispred s p\<^sub>\<rho> c \<and> \<not>eval \<and> pred
        \<Longrightarrow> P s (trans_ntmispred s p\<^sub>\<rho> c)\<rbrakk>
  \<Longrightarrow> \<lbrakk>s' = trans_ntpred s p\<^sub>\<rho> \<and> \<not>eval \<and> \<not>pred
        \<Longrightarrow> P s (trans_ntpred s p\<^sub>\<rho>)\<rbrakk>
  \<Longrightarrow> P s s'
"
  by (metis (full_types) 
            eval_pred.simps(1) 
            eval_pred.simps(2) 
            eval_pred.simps(3) 
            eval_pred.simps(4))


subsection \<open>Instruction Evaluation\<close>

fun eval_instr :: "MachineState \<Rightarrow> Address \<Rightarrow> instr \<Rightarrow> MachineState" where
"eval_instr s p\<^sub>\<rho> (RegisterUpdate r e) = trans_register_update s p\<^sub>\<rho> r e" |
"eval_instr s p\<^sub>\<rho> (Load r e) = trans_load s p\<^sub>\<rho> r e" |
"eval_instr s p\<^sub>\<rho> (Store e\<^sub>1 e\<^sub>2) = trans_store s p\<^sub>\<rho> e\<^sub>1 e\<^sub>2" |
"eval_instr s p\<^sub>\<rho> (Branch e c) = eval_pred s p\<^sub>\<rho> c (\<Delta> s, n s \<turnstile> e \<Down> air_true) (mispred (n s) (\<beta> s) (pc s))" |
"eval_instr s p\<^sub>\<rho> (Goto c) = trans_goto s p\<^sub>\<rho> c" |
"eval_instr s p\<^sub>\<rho> (SpecFence) = trans_specfence s"

lemma eval_instr_cases: 
"(s' = eval_instr s p\<^sub>\<rho> i)
  \<Longrightarrow> \<lbrakk>\<And> r e . s' = trans_register_update s p\<^sub>\<rho> r e \<Longrightarrow> i = RegisterUpdate r e 
        \<Longrightarrow> P s (trans_register_update s p\<^sub>\<rho> r e)\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> r e . s' = trans_load s p\<^sub>\<rho> r e \<Longrightarrow> i = Load r e 
        \<Longrightarrow> P s (trans_load s p\<^sub>\<rho> r e)\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> e\<^sub>1 e\<^sub>2 . s' = trans_store s p\<^sub>\<rho> e\<^sub>1 e\<^sub>2 \<Longrightarrow> i = Store e\<^sub>1 e\<^sub>2
        \<Longrightarrow> P s (trans_store s p\<^sub>\<rho> e\<^sub>1 e\<^sub>2)\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> e c . s' = (eval_pred s p\<^sub>\<rho> c (\<Delta> s, n s \<turnstile> e \<Down> air_true) (mispred (n s) (\<beta> s) (pc s))) \<Longrightarrow> i = Branch e c 
        \<Longrightarrow> P s (eval_pred s p\<^sub>\<rho> c (\<Delta> s, n s \<turnstile> e \<Down> air_true) (mispred (n s) (\<beta> s) (pc s)))\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> c . s' = trans_goto s p\<^sub>\<rho> c \<Longrightarrow> i = Goto c 
        \<Longrightarrow> P s (trans_goto s p\<^sub>\<rho> c)\<rbrakk>
  \<Longrightarrow> \<lbrakk>s' = trans_specfence s \<Longrightarrow> i = SpecFence 
        \<Longrightarrow> P s (trans_specfence s)\<rbrakk>
  \<Longrightarrow> P s s'
"
  by (metis eval_instr.simps(1) 
            eval_instr.simps(2) 
            eval_instr.simps(3) 
            eval_instr.simps(4) 
            eval_instr.simps(5) 
            eval_instr.simps(6)
            instr.exhaust)

subsection \<open>BIL (Binary Intermediate Language) abbreviations\<close>

definition register_update_alias :: "reg \<Rightarrow> exp \<Rightarrow> instr" ("_ := _") where
"(r := e) = RegisterUpdate r e"

lemma [simp]: "(r := e) = RegisterUpdate r e"
  by (simp add: register_update_alias_def)

definition load_alias :: "reg \<Rightarrow> exp \<Rightarrow> instr" ("_ := mem[_, el]:u64") where
"(r := mem[e, el]:u64) = Load r e"

lemma [simp]: "(r := mem[e, el]:u64) = Load r e"
  by (simp add: load_alias_def)

definition store_alias :: "exp \<Rightarrow> exp \<Rightarrow> instr" ("mem := mem with [_, el]:u64 <- _") where
"(mem := mem with [e\<^sub>1, el]:u64 <- e\<^sub>2) = Store e\<^sub>1 e\<^sub>2"

lemma [simp]: "(mem := mem with [e\<^sub>1, el]:u64 <- e\<^sub>2) = Store e\<^sub>1 e\<^sub>2"
  by (simp add: store_alias_def)

definition branch_alias :: "exp \<Rightarrow> const \<Rightarrow> instr" ("when _ goto _") where
"(when e goto c) = Branch e c"

lemma [simp]: "(when e goto c) = Branch e c"
  by (simp add: branch_alias_def)

definition goto_alias :: "const \<Rightarrow> instr"  ("goto _") where
"(goto c) = Goto c"

lemma [simp]: "(goto c) = Goto c"
  by (simp add: goto_alias_def)

end
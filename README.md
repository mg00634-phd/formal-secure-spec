
Documentation and proofs can be obtained with 

```
isabelle build -D .
```

To compile the functions use:
```
gcc ./vuln.c -o vuln
```

To generate the BAP use:
```
bap ./vuln -dbir --optimization-level=3 > examples.bil
```
theory AdversaryModel
  imports Main SystemModel
begin

section \<open>The Adversary Model\<close>

subsection \<open>Trusted Program and Confidential States\<close>

(* Extended state for Adversary, contains *)
record AdversaryMachineState = MachineState +
  (* Set of addresses pointing to trusted program instructions *)
  \<T>\<^sub>\<rho> :: "Address set"
  (* Entry point address for the trusted program instructions *)
  \<E>\<P> :: "Address"
  (* Addresses to the confidential memory states that need to be protected from the adversary *)
  \<S>\<^sub>\<T> :: "Address set"
  (* Attacker readable addresses *)
  \<U>rd :: "Address set"
  (* Attacker writable addresses *)
  \<U>wr :: "Address set"


(* Return all public memory in p\<^sub>\<mu>, ie not the memory in the secret set *)
definition "\<P>\<^sub>\<T> p\<^sub>\<mu> p\<^sub>\<S>\<^sub>\<T> \<equiv> (\<lambda>a . if a \<notin> p\<^sub>\<S>\<^sub>\<T> then the (p\<^sub>\<mu> 0) a else None)" 


(* High instruction executed in state s in the trusted set of addresses *)

definition "inst\<^sub>\<T> s \<equiv>
  let \<rho> = the (pc s 0) in
    if \<rho> \<in> \<T>\<^sub>\<rho> s
    then \<Pi> s \<rho>
    else None
"

(* High operation, combination of the public memory and high instruction *)

definition "op\<^sub>\<H>\<^sub>s s \<equiv> (inst\<^sub>\<T> s, \<P>\<^sub>\<T> (\<mu> s) (\<S>\<^sub>\<T> s))"

definition "op\<^sub>\<H>\<^sub>\<pi> \<pi> \<equiv> map (op\<^sub>\<H>\<^sub>s) \<pi>"


(* Low instruction/operation executed in state s not in the trusted set of addresses *)

definition "op\<^sub>\<L>\<^sub>s s \<equiv>
  let \<rho> = the (pc s 0) in
    if \<rho> \<in> \<T>\<^sub>\<rho> s
    then None
    else \<Pi> s \<rho>
"

definition "op\<^sub>\<L>\<^sub>\<pi> \<pi> \<equiv> map (op\<^sub>\<L>\<^sub>s) \<pi>"



subsection \<open>Adversary Constraints\<close>

subsubsection \<open>Conformant Store Addresses\<close>


definition "conformantStoreAddress \<pi> = (\<forall>s\<^sub>i . s\<^sub>i \<in> set \<pi> \<longrightarrow>
  (((n s\<^sub>i = 0) \<and> (the (pc s\<^sub>i 0) \<notin> \<T>\<^sub>\<rho> s\<^sub>i))
  \<longrightarrow> (case \<iota> s\<^sub>i of Store e\<^sub>1 _ \<Rightarrow> ((\<Delta> s\<^sub>i), 0 \<turnstile> e\<^sub>1) \<in> \<U>wr s\<^sub>i | _ \<Rightarrow> True))
)"


subsubsection \<open>Conformant Entrypoints\<close>

(* For all \<pi>\<^sub>i *)
definition "conformantEntrypoints \<pi> = (\<forall>i j. i < j \<and> j < length \<pi> \<and>
  (let \<pi>\<^sub>i = nth \<pi> i in
    (let \<pi>\<^sub>j = nth \<pi> j in 
      (((n \<pi>\<^sub>i) = 0) \<and> ((n \<pi>\<^sub>j) = 0))
      \<longrightarrow> (\<forall>k . i < k \<and> k < j \<longrightarrow> (n (nth \<pi> k)) \<noteq> 0)
      \<longrightarrow> (the ((pc \<pi>\<^sub>i) 0) \<notin> \<T>\<^sub>\<rho> \<pi>\<^sub>i \<and> the ((pc \<pi>\<^sub>j) 0) \<in> \<T>\<^sub>\<rho> \<pi>\<^sub>j)
      \<longrightarrow> (the ((pc \<pi>\<^sub>j) 0) = \<E>\<P> \<pi>\<^sub>j))))"


subsubsection \<open>Conformant Halting\<close>

text \<open>Conformant halting shows that a trace \<close>

definition "conformantHalt \<pi> = (let s = last \<pi> in halting s \<and> \<not> speculating s)"

lemma last_trace_no_trans:
  assumes "conformantHalt \<pi>"
  and "s = last \<pi>"
shows "\<not>(s \<leadsto> s')"
  apply (rule no_valid_transition_halt)
  using assms by (simp_all add: conformantHalt_def Let_def)

subsubsection \<open>Model of conformancy\<close>


text \<open>A trace is conformant if there is no speculation in the first state and the trusted component
has been initialized. Every state must also satisfy the conformantStoreAddress and 
conformantEntrypoints predicates\<close>


definition "conformant \<pi> \<equiv>
  let \<pi>\<^sub>0 = (hd \<pi>) in
    n \<pi>\<^sub>0 = 0
    \<and> conformantStoreAddress \<pi>
    \<and> conformantEntrypoints \<pi>
    \<and> conformantHalt (map MachineState.truncate \<pi>)
"

text \<open>TODO\<close>

definition "conformantTransitions \<pi> \<pi>\<^sub>x \<pi>\<^sub>y \<pi>\<^sub>z \<equiv>
  (\<forall>i . i < (length \<pi> - 1) \<longrightarrow> (
    let s = nth \<pi> i in
    let s\<^sub>x = nth \<pi>\<^sub>x i in
    let s\<^sub>y = nth \<pi>\<^sub>y i in
    let s\<^sub>z = nth \<pi>\<^sub>z i in
    let s' = nth \<pi> (Suc i) in
      if \<not> speculating s \<and> (speculating s\<^sub>x \<or> speculating s\<^sub>y \<or> speculating s\<^sub>z)
      then (s = s')
      else (s \<leadsto> s')
))"

text \<open>Any four traces are conformant together if \<close>


definition "conformant4 \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<equiv>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>2 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>3 = length \<pi>\<^sub>4 \<and>
  conformantTransitions \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<and>
  conformantTransitions \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<pi>\<^sub>1 \<and>
  conformantTransitions \<pi>\<^sub>3 \<pi>\<^sub>4 \<pi>\<^sub>2 \<pi>\<^sub>3 \<and>
  conformantTransitions \<pi>\<^sub>4 \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3
"


subsection \<open>Adversary Observations\<close>

text \<open>Low equivalence refers to how a low component in a system model must not be able to
distinguish between secret states of the high component. This can also be extended to traces such 
that every state within two traces is low equivalent. 

An adversary cannot observe speculative states (n > 0) as these cannot be output (except through 
side channels) and can only execute instructions in a non trusted state (\<rho> \<notin> \<T>\<rho>). They can therefore
read all non-speculative register and memory values () where the memory values must be attacker 
readable (). The branch predictor state and trace of past instruction and data memory accesses is 
trivial to access.\<close>


definition low_equiv_s :: "AdversaryMachineState \<Rightarrow> AdversaryMachineState \<Rightarrow> bool" 
    (infixl "\<approx>\<^sub>\<L>\<^sub>s" 100) where
"s \<approx>\<^sub>\<L>\<^sub>s s' = (
    (n s = 0 \<and> the (pc s 0) \<notin> \<T>\<^sub>\<rho> s)
    \<longrightarrow> (((\<Delta> s) 0 = (\<Delta> s') 0)
        \<and> (\<forall>a . a \<in> \<U>rd s \<longrightarrow> ((\<mu> s)[0, a]\<^sub>m) = ((\<mu> s')[0, a]\<^sub>m))
        \<and> \<omega> s = \<omega> s'
        \<and> \<beta> s = \<beta> s')
)"


fun low_equiv_t :: "AdversaryMachineState list \<Rightarrow> AdversaryMachineState list \<Rightarrow> bool"
    (infixl "\<approx>\<^sub>\<L>\<^sub>\<pi>" 100) where
"[] \<approx>\<^sub>\<L>\<^sub>\<pi> [] = True" |
"(h\<pi>\<^sub>1 # t\<pi>\<^sub>1) \<approx>\<^sub>\<L>\<^sub>\<pi> (h\<pi>\<^sub>2 # t\<pi>\<^sub>2) = ((h\<pi>\<^sub>1 \<approx>\<^sub>\<L>\<^sub>s h\<pi>\<^sub>2) \<and> (t\<pi>\<^sub>1 \<approx>\<^sub>\<L>\<^sub>\<pi> t\<pi>\<^sub>2))"

definition not_low_equiv_t :: "AdversaryMachineState list \<Rightarrow> AdversaryMachineState list \<Rightarrow> bool"
    (infixl "!\<approx>\<^sub>\<L>\<^sub>\<pi>" 100) where
"(x !\<approx>\<^sub>\<L>\<^sub>\<pi> y) = (\<not>(x \<approx>\<^sub>\<L>\<^sub>\<pi> y))"


end
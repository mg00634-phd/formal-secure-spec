theory AIRExpressions
  imports Main AIRSyntax
begin

section \<open>AIR Expressions\<close>


subsection \<open>Expression Semantics\<close>

primrec eval_exp :: "RegisterState \<Rightarrow> SpeculationLevel \<Rightarrow> exp \<Rightarrow> const" ("_, _ \<turnstile> _" [0, 10] 10) 
  where
"(_, _ \<turnstile> Const v) = v" |
"(p\<^sub>\<Delta>, p\<^sub>n \<turnstile> Reg r) = the (p\<^sub>\<Delta> [p\<^sub>n, r]\<^sub>m)" |
"(p\<^sub>\<Delta>, p\<^sub>n \<turnstile> UnOp f e) = f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e)" |
"(p\<^sub>\<Delta>, p\<^sub>n \<turnstile> BinOp e\<^sub>1 f e\<^sub>2) = f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>1) (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>2)" 

lemma eval_exp_cases: 
"(eval_exp p\<^sub>\<Delta> p\<^sub>n e = v')
  \<Longrightarrow> \<lbrakk>\<And> v . v' = v \<and> e = Const v 
        \<Longrightarrow> P e v\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> r . v' = the (p\<^sub>\<Delta> [p\<^sub>n, r]\<^sub>m) \<and> e = Reg r
        \<Longrightarrow> P e (the (p\<^sub>\<Delta> [p\<^sub>n, r]\<^sub>m))\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> f e' . v' = f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e') \<and> e = UnOp f e' 
        \<Longrightarrow> P e (f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e'))\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And> f e\<^sub>1 e\<^sub>2 . v' = f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>1) (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>2) \<and> e = BinOp e\<^sub>1 f e\<^sub>2
        \<Longrightarrow> P e (f (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>1) (p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e\<^sub>2))\<rbrakk>
  \<Longrightarrow> P e v'
"
  by (metis eval_exp.simps(1) 
            eval_exp.simps(2) 
            eval_exp.simps(3) 
            eval_exp.simps(4) 
            exp.exhaust)

(* evaluation syntax from the paper *)
definition eval_exp_value :: "RegisterState \<Rightarrow> SpeculationLevel \<Rightarrow> exp \<Rightarrow> const \<Rightarrow> bool" 
      ("_, _ \<turnstile> _ \<Down> _" [0, 10] 10) where
"(p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e \<Down> v) = ((p\<^sub>\<Delta>, p\<^sub>n \<turnstile> e) = v)"

(* prove the papers evaluation syntax *)
lemma [simp]: "a, b \<turnstile> e \<Down> v \<Longrightarrow> (a, b \<turnstile> e) = v"
  by (simp add: eval_exp_value_def)

subsection \<open>Prediction indicators\<close>

definition "t_pred     s e \<equiv> (\<Delta> s, n s \<turnstile> e \<Down> air_true) \<and> \<not>mispred (n s) (\<beta> s) (pc s)"
definition "t_mispred  s e \<equiv> (\<Delta> s, n s \<turnstile> e \<Down> air_true) \<and> mispred (n s) (\<beta> s) (pc s)"
definition "nt_pred    s e \<equiv> \<not>(\<Delta> s, n s \<turnstile> e \<Down> air_true) \<and> \<not>mispred (n s) (\<beta> s) (pc s)"
definition "nt_mispred s e \<equiv> \<not>(\<Delta> s, n s \<turnstile> e \<Down> air_true) \<and> mispred (n s) (\<beta> s) (pc s)"

lemmas preds = t_pred_def t_mispred_def nt_pred_def nt_mispred_def

subsection \<open>AIR compatible Binary and Unary operations\<close>

fun lshift :: "int \<Rightarrow> int \<Rightarrow> int" (infixl "<<" 150) where
"(a << b) = (if b \<le> 0 then a else ((a * 2) << (b - 1)))"

definition "asm_lt a b \<equiv> if a < b then 1 else 0"
definition "asm_and a b \<equiv> a"
definition "asm_eq a b \<equiv> if a = b then 1 else 0"


end

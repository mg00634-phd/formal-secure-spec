theory SystemModel
    imports Main AIRInstructions
begin

section \<open>System Model for Speculative AIR\<close>

subsection \<open>Valid Transition\<close>

definition valid_trans :: "MachineState \<Rightarrow> MachineState \<Rightarrow> bool" (infixl "\<leadsto>" 101) where
"s \<leadsto> s' \<equiv> 
  let \<rho> = \<rho> s in
    if resolve (n s) (\<beta> s) (pc s) then 
      s' = trans_resolve s \<rho>
    else \<not>halting s \<and> s' = eval_instr s \<rho> (\<iota> s)"

text \<open>Case simplification for the valid transition\<close>

lemma valid_trans_cases:
"s \<leadsto> s'
  \<Longrightarrow> \<lbrakk>resolve (n s) (\<beta> s) (pc s)
          \<Longrightarrow> s' = trans_resolve s (\<rho> s)
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>e c. \<not> resolve (n s) (\<beta> s) (pc s)
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Branch e c 
          \<Longrightarrow> s' = trans_tmispred s (\<rho> s) c 
          \<Longrightarrow> \<Delta> s, n s \<turnstile> e \<Down> air_true  
          \<Longrightarrow>  mispred (n s) (\<beta> s) (pc s)
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>e c. \<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Branch e c 
          \<Longrightarrow> s' = trans_tpred s (\<rho> s) c 
          \<Longrightarrow> (\<Delta> s, n s \<turnstile> e \<Down> air_true) 
          \<Longrightarrow> \<not> mispred (n s) (\<beta> s) (pc s)
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>e c. \<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Branch e c
          \<Longrightarrow> s' = trans_ntmispred s (\<rho> s) c 
          \<Longrightarrow> \<not> (\<Delta> s, n s \<turnstile> e \<Down> air_true) 
          \<Longrightarrow> mispred (n s) (\<beta> s) (pc s)
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>e c. \<not> resolve (n s) (\<beta> s) (pc s)
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Branch e c 
          \<Longrightarrow> s' = trans_ntpred s (\<rho> s) 
          \<Longrightarrow> \<not> (\<Delta> s, n s \<turnstile> e \<Down> air_true)
          \<Longrightarrow> \<not> mispred (n s) (\<beta> s) (pc s)
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>c. \<not> resolve (n s) (\<beta> s) (pc s)
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Goto c
          \<Longrightarrow> s' = trans_goto s (\<rho> s) c 
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = SpecFence
          \<Longrightarrow> s' = trans_specfence s
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>r e. \<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = RegisterUpdate r e
          \<Longrightarrow> s' = trans_register_update s (\<rho> s) r e
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>r e. \<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Load r e
          \<Longrightarrow> s' = trans_load s (\<rho> s) r e 
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> \<lbrakk>\<And>e\<^sub>1 e\<^sub>2. \<not> resolve (n s) (\<beta> s) (pc s) 
          \<Longrightarrow> \<not> halting s
          \<Longrightarrow> \<iota> s = Store e\<^sub>1 e\<^sub>2
          \<Longrightarrow> s' = trans_store s (\<rho> s) e\<^sub>1 e\<^sub>2
        \<Longrightarrow> P s s'\<rbrakk>
  \<Longrightarrow> P s s'"

  apply (simp add: valid_trans_def Let_def)
  apply (case_tac "resolve (n s) (\<beta> s) (pc s)", simp_all)
  apply (case_tac "halting s", simp_all)
  apply (erule eval_instr_cases)
  defer defer defer
  apply (erule eval_pred_cases)
  by (simp_all add: halting_def)



subsubsection \<open>Program memory (\<Pi>)\<close>

text \<open>The program memory is always invariant.\<close>

lemma const_\<Pi>: 
  assumes "s \<leadsto> s'"
    shows "\<Pi> s' = \<Pi> s"
  using assms(1) apply (rule valid_trans_cases)
  by (simp_all add: Let_def trans_tmispred_def trans_tpred_def trans_ntmispred_def trans_ntpred_def 
                    trans_register_update_def trans_specfence_def trans_load_def trans_store_def 
                    trans_goto_def trans_resolve_def)



subsubsection \<open>Register state (\<Delta>)\<close>

text \<open>Register state is invariant on Resolve and during the SpecFence, Goto and Store instructions.
      Also invariant on the Branch instruction given we do not miss predict.\<close>

lemma const_\<Delta>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s) \<longrightarrow> \<iota> s \<in> {Store e\<^sub>1 e\<^sub>2, Branch e c, Goto c, SpecFence}"
  and "(\<not> resolve (n s) (\<beta> s) (pc s) \<and> \<iota> s = Branch e c) \<longrightarrow> \<not> mispred (n s) (\<beta> s) (pc s)"
shows "\<Delta> s' = \<Delta> s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: trans_resolve_def Let_def trans_store_def trans_goto_def
                                trans_specfence_def trans_tpred_def trans_ntpred_def)

text \<open>Updating a register (r) with the RegisterUpdate instruction sets its value to v.\<close>

lemma \<Delta>_n_r_v:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = RegisterUpdate r e"
shows "\<Delta> s' = ((\<Delta> s)[n s', r]\<^sub>m \<mapsto> (\<Delta> s, n s \<turnstile> e))"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: trans_register_update_def Let_def)

text \<open>Updating a register (r) with the Load instruction sets its value to that of the data located 
      at address a in \<mu>.\<close>

lemma \<Delta>_n_r_v_at_a:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Load r e"
shows "\<Delta> s' = ((\<Delta> s)[n s', r]\<^sub>m \<mapsto> (the ((\<mu> s) [n s, (\<Delta> s, n s \<turnstile> e)]\<^sub>m)))"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: trans_load_def Let_def)

text \<open>If miss prediction occurs on a Branch instruction then the registers at the new spec level 
      equal the registers at the old spec level.\<close>

lemma \<Delta>_n_equals_\<Delta>'_n':
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Branch e c"
  and "mispred (n s) (\<beta> s) (pc s)"
  and "wf\<Delta> s"
shows "\<Delta> s' = \<Delta> s (n s' \<mapsto> the ((\<Delta> s)(n s)))"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def wf\<Delta>_def trans_tmispred_def trans_ntmispred_def)

lemmas \<Delta>_trans = const_\<Delta> \<Delta>_n_r_v \<Delta>_n_r_v_at_a \<Delta>_n_equals_\<Delta>'_n'



subsubsection \<open>Data memory (\<mu>)\<close>

text \<open>Data memory is invariant on Resolve and during the SpecFence, Goto and Store instructions.
      Also invariant on the Branch instruction given we do not miss predict.\<close>

lemma const_\<mu>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s) \<longrightarrow> \<iota> s \<in> {RegisterUpdate r e, Load r e, Branch e c, Goto c, SpecFence}"
  and "(\<not> resolve (n s) (\<beta> s) (pc s) \<and> \<iota> s = Branch e c) \<longrightarrow> \<not> mispred (n s) (\<beta> s) (pc s)"
shows "\<mu> s' = \<mu> s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_tpred_def trans_ntpred_def trans_load_def 
                                trans_goto_def trans_specfence_def trans_resolve_def 
                                trans_register_update_def)

text \<open>Updating a memory address (a) with the Store instruction sets its value to that of the value 
      v\<close>

lemma \<mu>_n_a_v:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Store e\<^sub>1 e\<^sub>2"
shows "(\<mu> s') = ((\<mu> s)[n s', (\<Delta> s, n s \<turnstile> e\<^sub>1)]\<^sub>m \<mapsto> (\<Delta> s, n s \<turnstile> e\<^sub>2))"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_store_def)

text \<open>If miss prediction occurs on a Branch instruction then the memory at the new spec level equal 
      the memory at the old spec level.\<close>

lemma \<mu>_n_equals_\<mu>'_n':
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Branch e c"
  and "mispred (n s) (\<beta> s) (pc s)"
  and "wf\<mu> s"
shows "\<mu> s' = ((\<mu> s)(n s' \<mapsto> the ((\<mu> s)(n s))))"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def wf\<mu>_def trans_tmispred_def trans_ntmispred_def)

lemmas \<mu>_trans = const_\<mu> \<mu>_n_a_v \<mu>_n_equals_\<mu>'_n'



subsubsection \<open>Program Counter (pc)\<close>

text \<open>Miss-predicted false evaluation sets the program counter at the new speculation level to c\<close>

lemma const_pc:
  assumes "s \<leadsto> s'" 
  and "\<not> resolve (n s) (\<beta> s) (pc s) \<longrightarrow> \<iota> s = SpecFence"
shows "pc s' = pc s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_specfence_def trans_resolve_def)

text \<open>Predicted false evaluation sets the program counter at the current speculation level to \<rho> + 1\<close>

lemma pc_n'_next_\<rho>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s \<in> {RegisterUpdate r e, Load r e, Store e\<^sub>1 e\<^sub>2, Branch e c}"
  and "\<iota> s = Branch e c \<longrightarrow> nt_pred s e"
shows "pc s' = (pc s)(n s' \<mapsto> \<rho> s + 1)"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_register_update_def trans_load_def trans_store_def 
                                trans_ntpred_def nt_pred_def)

text \<open>Predicted true evaluation sets the program counter at the current speculation level to c\<close>

lemma pc_n'_c:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s \<in> {Goto c, Branch e c}"
  and "\<iota> s = Branch e c \<longrightarrow> t_pred s e"
shows "pc s' = (pc s)(n s' \<mapsto> c)"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_tpred_def trans_goto_def t_pred_def)

text \<open>Miss-predicted true evaluation sets the program counter at the new speculation level to \<rho> + 1
      and to c at the current speculation level.\<close>

lemma pc_n_c_n'_next_\<rho>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Branch e c"
  and "t_mispred s e"
shows "pc s' = (pc s)([n s, n s'] [\<mapsto>] [c, \<rho> s + 1])"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_tmispred_def t_mispred_def)

text \<open>Miss-predicted false evaluation sets the program counter at the new speculation level to c and
      to \<rho> + 1 at the current speculation level.\<close>

lemma pc_n_next_\<rho>_n'_c:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Branch e c"
  and "nt_mispred s e"
shows "pc s' = (pc s)([n s, n s'] [\<mapsto>] [\<rho> s + 1, c])"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (auto simp add: Let_def trans_ntmispred_def nt_mispred_def)

lemmas pc_trans = const_pc pc_n'_next_\<rho> pc_n_c_n'_next_\<rho> pc_n'_c pc_n_next_\<rho>_n'_c



subsubsection \<open>Trace of instructions and memory accessed (\<omega>)\<close>

text \<open>Resolve and specfence instructions do not leave a trace.\<close>

lemma const_\<omega>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s) \<longrightarrow> \<iota> s = SpecFence"
shows "\<omega> s' = \<omega> s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_resolve_def trans_specfence_def)

text \<open>Instructions which do not access memory only add their program address to the trace.\<close>

lemma \<omega>_\<rho>_None:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s \<in> {RegisterUpdate r e, Branch e c, Goto c}"
shows "\<omega> s' = \<omega> s @ [(\<rho> s, None)]"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_resolve_def trans_goto_def trans_register_update_def 
                                trans_tmispred_def trans_tpred_def trans_ntmispred_def 
                                trans_ntpred_def \<rho>_def)

text \<open>Instructions that access memory add this to the trace along with their program address.\<close>

lemma \<omega>_\<rho>_a:
  assumes "s \<leadsto> s'"
  and "\<iota> s \<in> {Load r e, Store e e\<^sub>2}"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "a = (\<Delta> s, n s \<turnstile> e)"
shows "\<omega> s' = \<omega> s @ [(\<rho> s, Some a)]"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_store_def trans_load_def \<rho>_def)

lemmas \<omega>_trans = const_\<omega> \<omega>_\<rho>_None \<omega>_\<rho>_a



subsubsection \<open>Branch predictor (\<beta>)\<close>

text \<open>Non branching instructions do not change the branch predictor.\<close>

lemma const_\<beta>:
  assumes "s \<leadsto> s'"
  and "\<iota> s \<in> {RegisterUpdate r e, Load r e, Store e\<^sub>1 e\<^sub>2, SpecFence}"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
shows "\<beta> s' = \<beta> s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_register_update_def trans_load_def trans_store_def 
                                trans_specfence_def)

text \<open>Branching instructions add accesses to the branch predictor.\<close>

lemma update_\<beta>:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s) \<longrightarrow> \<iota> s \<in> {Branch e c, Goto c}"
shows "\<beta> s' = \<beta> s @ [(n s, \<rho> s)]"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_resolve_def trans_goto_def update_def trans_ntpred_def
                  trans_tmispred_def trans_tpred_def trans_ntmispred_def)

lemmas \<beta>_trans = const_\<beta> update_\<beta>



subsubsection \<open>Speculation level (n)\<close>

text \<open>Correct prediction and non spec fence instructions do not change the spec level.\<close>

lemma const_n:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s \<noteq> SpecFence"
  and "(\<And>e c . \<iota> s = Branch e c \<longrightarrow> \<not>mispred (n s) (\<beta> s) (pc s))"
shows "n s' = n s"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_register_update_def trans_load_def trans_store_def 
                                trans_goto_def trans_tpred_def trans_ntpred_def)

text \<open>Only speculate on mispredictions.\<close>

lemma inc_n_mispred:
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = Branch e c"
  and "mispred (n s) (\<beta> s) (pc s)"
shows "n s' = n s + 1"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: Let_def trans_tmispred_def trans_ntmispred_def)

text \<open>Specfence resets all speculation.\<close>

lemma n_0_fence: 
  assumes "s \<leadsto> s'"
  and "\<not> resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s = SpecFence"
shows "n s' = 0"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: trans_specfence_def Let_def)

text \<open>Resolve will resolve a miss-predicted branch by reducing the speculation level.\<close>

lemma dec_n_resolve: 
  assumes "s \<leadsto> s'" 
  and "resolve (n s) (\<beta> s) (pc s)"
shows "n s' = n s - 1"
  using assms(1) apply (rule valid_trans_cases)
  using assms(2) by (simp_all add: trans_resolve_def Let_def)

lemmas n_trans = const_n n_0_fence dec_n_resolve inc_n_mispred



subsection \<open>Lemmas for the System Model\<close>

text \<open>Never resolve if executing speculatively.\<close>

lemma never_resolve_not_speculating:
  assumes "s \<leadsto> s'"
  and "\<not>speculating s"
shows "\<not>resolve (n s) (\<beta> s) (pc s)"
  using assms by (simp add: valid_trans_def speculating_def)

text \<open>Only resolve if speculating.\<close>

lemma resolve_must_be_speculating:
  assumes "s \<leadsto> s'"
  and "resolve (n s) (\<beta> s) (pc s)"
shows "speculating s"
  using assms(1) assms(2) never_resolve_not_speculating by blast

text \<open>If halting but still speculating always resolve.\<close>

lemma always_resolve_on_halt:
  assumes "s \<leadsto> s'"
  and "speculating s"
  and "halting s"
shows "resolve (n s) (\<beta> s) (pc s)"
  using assms apply (simp add: speculating_def valid_trans_def Let_def)
  by meson

text \<open>If halting then no valid transition.\<close>

lemma no_valid_transition_halt:
  assumes "\<not> speculating s"
  and "halting s"
shows "\<not> s \<leadsto> s'"
  apply (rule notI)  
  using assms by (simp add: valid_trans_def speculating_def)

text \<open>Performing a jump to HALT will halt the program counter in the next state\<close>

lemma jmp_halt:
  assumes "s \<leadsto> s'"
  and "\<not>resolve (n s) (\<beta> s) (pc s)"
  and "\<iota> s \<in> {Goto HALT, Branch e HALT}"
  and "\<iota> s = Branch e HALT \<longrightarrow> (t_pred s e \<or> nt_mispred s e)"
shows "halting s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: t_pred_def nt_mispred_def pc_trans halting_def \<rho>_def)



subsection \<open>Well formed transitions\<close>

text \<open>These lemmas prove that s' is always well formed given a valid transition from s to s' where s
      is a well formed state.\<close>
       
lemma wf\<Pi>_trans: 
  assumes "s \<leadsto> s'"
  and "wf\<Pi> s"
shows "wf\<Pi> s'"
  using assms by (simp add: wf\<Pi>_def const_\<Pi>)

lemma wf\<Delta>_trans: 
  assumes "s \<leadsto> s'"
  and "wf\<Delta> s"
shows "wf\<Delta> s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: wf\<Delta>_def n_trans \<Delta>_trans) 

lemma wf\<mu>_trans:
  assumes "s \<leadsto> s'"
  and "wf\<mu> s"
shows "wf\<mu> s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: wf\<mu>_def n_trans \<mu>_trans)

lemma wfpc_trans: 
  assumes "s \<leadsto> s'"
  and "wfpc s"
  and "wf\<Pi> s"
shows "wfpc s'"
  using assms apply (simp_all add: wfpc_def wf\<rho>_def Let_def)
  apply (intro allI impI conjI)
  using assms(1) apply (rule valid_trans_cases)
  prefer 11 using assms(1) apply (rule valid_trans_cases)
  apply (simp_all add: const_\<Pi> n_trans pc_trans halting_def preds)
  using le_Suc_eq apply (simp_all add: wf\<Pi>_def wf\<rho>_in_\<Pi>_def \<rho>_def \<iota>_def)
  apply (rule conjI)
  apply (metis domIff option.exhaust_sel ranI wf\<iota>_in_\<Pi>.simps(1) wf\<rho>_def)
  apply (metis instr.simps(39) le_refl)
  apply (metis domIff option.exhaust_sel ranI wf\<iota>_in_\<Pi>.simps(1) wf\<rho>_def)
  apply (rule conjI)
  apply (metis instr.simps(39))
  apply (metis domIff le_refl option.exhaust_sel ranI wf\<iota>_in_\<Pi>.simps(1) wf\<rho>_def)
  apply (metis instr.simps(39))
  apply (metis domIff option.exhaust_sel ranI wf\<iota>_in_\<Pi>.simps(2) wf\<rho>_def)
  apply (metis instr.simps(36))
  apply (metis instr.simps(37))
  apply (metis instr.simps(38))
  done

lemma wf\<omega>_trans: 
  assumes "s \<leadsto> s'"
  and "wf\<omega> s"
  and "wfpc s"
shows "wf\<omega> s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (auto simp add: wf\<omega>_def wfpc_def wf\<rho>_def const_\<Pi> \<omega>_trans n_trans \<rho>_def halting_def 
                                 Let_def)

lemma wf\<beta>_trans: 
  assumes "s \<leadsto> s'"
  and "wfpc s"
  and "wf\<beta> s"
shows "wf\<beta> s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (auto simp add: wf\<beta>_def wfpc_def wf\<rho>_def const_\<Pi> \<beta>_trans n_trans \<rho>_def Let_def)

lemma wfn_trans: 
  assumes "s \<leadsto> s'"
  and "wfn s"
shows "wfn s'"
  using assms(1) apply (rule valid_trans_cases)
  using assms by (simp_all add: wfn_def)

lemma wfs_trans: 
  assumes "s \<leadsto> s'"
  and "wfs s"
shows "wfs s'"
  using assms by (simp add: wfs_def wf\<Pi>_trans wf\<Delta>_trans wf\<mu>_trans wfpc_trans wf\<omega>_trans wf\<beta>_trans
                            wfn_trans)

lemma wf\<iota>_wfs:
  assumes "wfs s"
shows "wf\<iota> s"
  using assms apply (simp_all add: wfs_def wf\<iota>_def wf\<Pi>_def wfpc_def wf\<rho>_def Let_def
                                   \<iota>_def \<rho>_def
                                   halting_def)
  by (meson domIff option.exhaust_sel order_refl ranI)

lemma wf\<iota>_trans:
  assumes "s \<leadsto> s'"
  and "wfs s"
shows "wf\<iota> s'"
  using assms(1) assms(2) by (simp add: wf\<iota>_wfs wfs_trans)

lemmas wf_trans = wf\<iota>_trans wfs_trans

end

theory Ex10
  imports ExBase "../TPOD"
begin 

definition "ex10_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,

   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RCX := (Reg RDI),
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> v1 := (Reg RCX),
   9 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v1)),
  10 \<mapsto> RAX := mem[Reg RAX, el]:u64,
  12 \<mapsto> v2 := (BinOp (Reg RSI) (-) (Reg RAX)),
  13 \<mapsto> ZF := (BinOp (Const 0) (asm_eq) (Reg v2)),
   2 \<mapsto> when (Reg ZF) goto 4,
   3 \<mapsto> goto 19,

   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  18 \<mapsto> goto 19,

  19 \<mapsto> goto HALT
]"

end

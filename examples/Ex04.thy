theory Ex03
  imports ExBase "../TPOD"
begin 

definition "ex04_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,

   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RCX := (Reg RDI),
   9 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Reg 4)),
  10 \<mapsto> v1 := (Reg RCX),
  11 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v1)),
  12 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  13 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Reg 12)),
  14 \<mapsto> v2 := (Reg RDX),
  15 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v2)),
  16 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  17 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  18 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  19 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),

  20 \<mapsto> goto HALT
]"

end
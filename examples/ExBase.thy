theory ExBase
   imports Main "../SystemModel"
begin

(* TODO invariants that don't equal each other*)
definition "array1_addr \<equiv> 0"
definition "array2_addr \<equiv> 1"
definition "temp_addr \<equiv> 2"
definition "array1_size_addr \<equiv> 3"
definition "array_size_mask_addr \<equiv> 4"
definition "last_x_addr \<equiv> 5"

end

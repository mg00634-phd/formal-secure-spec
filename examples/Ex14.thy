theory Ex14
  imports ExBase "../TPOD"
begin 


definition "ex01_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,

   1 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RCX := (Reg RDI),
   6 \<mapsto> RCX := (Reg RCX) XOR 0xFF,
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> v274 := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v274)),
  10 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> v284 := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v284)),
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  18 \<mapsto> goto 2,

  2 \<mapsto> goto HALT        
]"

end

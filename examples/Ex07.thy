theory Ex07
  imports ExBase "../TPOD"
begin 

definition "ex07_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const last_x_addr, el]:u64,
   1 \<mapsto> v1 := (BinOp (Reg RDI) (-) (Reg RAX)),
   1 \<mapsto> ZF := (BinOp (Const 0) (asm_eq) (Reg v1)),
   2 \<mapsto> when (Reg ZF) goto 3,
   3 \<mapsto> goto 18,


   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RCX := (Reg RDI),
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> v1 := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v1)),
  10 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> v2 := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v2)),
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
 
  18 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
  19 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
  20 \<mapsto> when (Reg CF) goto 24,

  21 \<mapsto> RAX := (Reg RDI),
  22 \<mapsto> mem := mem with [Const last_x_addr, el]:u64 <- (Reg RAX),

  24 \<mapsto> goto HALT
]"
end

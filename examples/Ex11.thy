theory Ex11
  imports ExBase "../TPOD"
begin 


(*TODO look an unoptimized code*)

definition "ex11_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,


   4 \<mapsto> RDX := mem[Const temp_addr, el]:u64,
   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RCX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RSI := (Reg RDI),
   7 \<mapsto> RSI := (BinOp (Reg RSI) (<<) (Const 3)),
   8 \<mapsto> v1 := (Reg RSI)
   9 \<mapsto> RCX := (BinOp (Reg RCX) (+) (Reg v1)),
  10 \<mapsto> RCX := mem[Reg RCX, el]:u64,
  11 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 12)),
  12 \<mapsto> v2 := (Reg RCX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v2)),
  14 \<mapsto> RAX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  18 \<mapsto> goto 18,

  19 \<mapsto> goto HALT
]"

end


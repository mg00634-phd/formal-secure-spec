theory Ex01
  imports ExBase "../TPOD"
begin
(*TODO image the system model*)

section \<open>Original Spectre BCB (Bounds Check Bypass)\<close>

text \<open>Example 1 of Paul Kocher's list of of 15 Spectre victim functions.\<close>

text \<open>Without speculative fence.\<close>


definition "ex01_\<Pi>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 4,
   3 \<mapsto> goto 19,

   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RCX := (Reg RDI),
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> (V 274) := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg (V 274))),
  10 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> (V 284) := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg (V 284))),
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  18 \<mapsto> goto 19,

  19 \<mapsto> goto HALT
]"

definition "half s input array1 array2 temp array1_size array_size_mask \<equiv> 
  let prog_mem = ex01_\<Pi>_vulnerable in
    let \<rho> = 0 in
      s = \<lparr>
        \<Pi> = prog_mem,
        \<Delta> = [0 \<mapsto> [RDI \<mapsto> input]],
        \<mu> = [0 \<mapsto> [array1_addr \<mapsto> array1,
                   array2_addr \<mapsto> array2,
                   temp_addr \<mapsto> temp,
                   array1_size_addr \<mapsto> array1_size,
                   array_size_mask_addr \<mapsto> array_size_mask]],
        pc = [0 \<mapsto> \<rho>],
        \<omega> = [],
        \<beta> = [],
        n = 0
      \<rparr>
"

definition "init_vulnerable\<^sub>\<T> s input array1 array2 temp array1_size array_size_mask \<equiv> 
  let prog_mem = ex01_\<Pi>_vulnerable in
    let \<rho> = 0 in
      s = \<lparr>
        \<Pi> = prog_mem,
        \<Delta> = [0 \<mapsto> [RDI \<mapsto> input]],
        \<mu> = [0 \<mapsto> [array1_addr \<mapsto> array1,
                   array2_addr \<mapsto> array2,
                   temp_addr \<mapsto> temp,
                   array1_size_addr \<mapsto> array1_size,
                   array_size_mask_addr \<mapsto> array_size_mask]],
        pc = [0 \<mapsto> \<rho>],
        \<omega> = [],
        \<beta> = [],
        n = 0,
        \<T>\<^sub>\<rho> = {1..18},
        \<E>\<P> = 1,
        \<S>\<^sub>\<T> = {array1_addr, array2_addr},
        \<U>rd = {array1_size_addr},
        \<U>wr = {}
      \<rparr>
"

lemma init_vulnerable_wfs: 
  assumes "init_vulnerable\<^sub>\<T> s input array1 array2 temp array1_size array_size_mask"
  shows "wfs s"
  apply (simp add: wfs_def)
  apply (intro conjI)
  using assms apply (simp_all add: init_vulnerable\<^sub>\<T>_def ex01_\<Pi>_vulnerable_def)
  apply (simp add: wf\<Pi>_def wf\<rho>_def wf\<rho>_in_\<Pi>_def HALT_def)
  apply (simp add: wf\<Delta>_def)
  apply (simp add: wf\<mu>_def)
  apply (simp add: wfpc_def Let_def wf\<rho>_def)
  apply (simp add: wf\<omega>_def)
  apply (simp add: wf\<beta>_def)
  apply (simp add: wfn_def)
  done

lemma [simp]:
  assumes "half s input array1 array2 temp array1_size array_size_mask"
  and "wfs s"
  and "s \<leadsto> s1"
  and "s1 \<leadsto> s2"
  and "s2 \<leadsto> s3"
  and "s3 \<leadsto> s4"
  and "s4 \<leadsto> s5"
  and "s5 \<leadsto> s6"
shows "the (pc s1 (n s1)) = (1::int) \<and> \<iota> s1 = (RegisterUpdate CF (BinOp (Reg RDI) asm_lt (Reg RAX))) \<and>
       the (pc s2 (n s2)) = (2::int)"
  using assms(3) apply (rule valid_trans_cases)
  using assms apply auto
  apply (simp_all add: const_\<Pi> const_n pc_n'_next_\<rho>)
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  defer
  apply (simp_all add: \<iota>_def \<rho>_def half_def ex01_\<Pi>_vulnerable_def)
  using assms(3) apply (rule valid_trans_cases)
  apply (simp_all add: const_\<Pi> const_n pc_n'_next_\<rho> \<rho>_def)
  defer defer defer defer defer defer defer defer
  apply (simp_all add: \<iota>_def \<rho>_def half_def ex01_\<Pi>_vulnerable_def)
  done

lemma [simp]:
  assumes "init_vulnerable\<^sub>\<T> s input array1 array2 temp array1_size array_size_mask"
  and "init_vulnerable\<^sub>\<T> s' input array1 array2 temp array1_size array_size_mask"
  and "MachineState.truncate s \<leadsto> MachineState.truncate s1"
  and "MachineState.truncate s' \<leadsto> MachineState.truncate s1'"
shows "\<iota> s = Load RAX (Const array1_size_addr)"
  using assms apply (simp add: \<iota>_def \<rho>_def low_equiv_s_def init_vulnerable\<^sub>\<T>_def ex01_\<Pi>_vulnerable_def)
  done

(* Custom induction rule*)

theorem ex01_vulnerable: 
  assumes "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4"
  and "init_vulnerable\<^sub>\<T> (hd \<pi>\<^sub>1) input array1 array2 temp array1_size array_size_mask"
  and "init_vulnerable\<^sub>\<T> (hd \<pi>\<^sub>2) input array1 array2 temp array1_size array_size_mask"
  and "init_vulnerable\<^sub>\<T> (hd \<pi>\<^sub>3) input array1 array2 temp array1_size array_size_mask"
  and "init_vulnerable\<^sub>\<T> (hd \<pi>\<^sub>4) input array1 array2 temp array1_size array_size_mask"
shows "\<pi>\<^sub>3 !\<approx>\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4"
  apply (simp add: not_low_equiv_t_def)
  apply (rule notI)
  using assms(1) apply (simp add: tpod_conformant_def)
  apply (simp add: conformant4_def)
  apply(induct "\<pi>\<^sub>3")
  apply(induct "\<pi>\<^sub>4")
  defer defer
  apply(induct "\<pi>\<^sub>4")
     apply (simp_all)
  apply (auto)
  apply (case_tac "\<pi>\<^sub>3", simp_all)
   apply (auto)
   defer
  using assms apply (simp add: tpod_conformant_def init_vulnerable\<^sub>\<T>_def ex01_\<Pi>_vulnerable_def
                               conformant_def conformantStoreAddress_def conformantEntrypoints_def 
                               conformantTrace_def Let_def)
  apply (auto)

  sorry


subsection \<open>Implementing a fix\<close>

definition "ex01_\<Pi>_safe \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 4,
   3 \<mapsto> goto 20,

   4 \<mapsto> SpecFence,
   5 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   6 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   7 \<mapsto> RCX := (Reg RDI),
   8 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   9 \<mapsto> (V 274) := (Reg RCX),
  10 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg (V 274))),
  11 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  12 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  13 \<mapsto> (V 284) := (Reg RDX),
  14 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg (V 284))),
  15 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  16 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  17 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  18 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  19 \<mapsto> goto 20,

  20 \<mapsto> goto HALT        
]"


text \<open>With speculative fence.\<close>


definition "init_safe\<^sub>\<T> s input array1 array2 temp array1_size array_size_mask \<equiv>
  let prog_mem = ex01_\<Pi>_safe in
    let \<rho> = 0 in
      s = \<lparr>
        \<Pi> = prog_mem,
        \<Delta> = [0 \<mapsto> [RDI \<mapsto> input]],
        \<mu> = [0 \<mapsto> [array1_addr \<mapsto> array1,
                   array2_addr \<mapsto> array2,
                   temp_addr \<mapsto> temp,
                   array1_size_addr \<mapsto> array1_size,
                   array_size_mask_addr \<mapsto> array_size_mask]],
        pc = [0 \<mapsto> \<rho>],
        \<omega> = [],
        \<beta> = [],
        n = 0,
        \<T>\<^sub>\<rho> = {4..18},
        \<E>\<P> = 4,
        \<S>\<^sub>\<T> = {array1_addr, array2_addr},
        \<U>rd = {array1_size_addr},
        \<U>wr = {}
      \<rparr>
"

lemma init_safe_wfs: 
  assumes "init_safe\<^sub>\<T> s input array1 array2 temp array1_size array_size_mask"
  shows "wfs s"
  apply (simp add: wfs_def)
  apply (intro conjI)
  using assms apply (simp_all add: init_safe\<^sub>\<T>_def ex01_\<Pi>_safe_def)
  apply (simp add: wf\<Pi>_def wf\<rho>_def wf\<rho>_in_\<Pi>_def HALT_def)
  apply (simp add: wf\<Delta>_def)
  apply (simp add: wf\<mu>_def)
  apply (simp add: wfpc_def Let_def wf\<rho>_def)
  apply (simp add: wf\<omega>_def)
  apply (simp add: wf\<beta>_def)
  apply (simp add: wfn_def)
  done

text \<open>Adding a SpecFence immediately after branching should prevent misspeculation from occuring. \<close>

lemma 
  assumes "\<pi>\<^sub>3 \<approx>\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4"
  and "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4"
shows "" 
  by auto



theorem ex01_nv_safe: 
  assumes "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4"
  and "init_safe\<^sub>\<T> (hd \<pi>\<^sub>1) input array1 array2 temp array1_size array_size_mask"
  and "init_safe\<^sub>\<T> (hd \<pi>\<^sub>2) input array1 array2 temp array1_size array_size_mask"
  and "init_safe\<^sub>\<T> (hd \<pi>\<^sub>3) input array1 array2 temp array1_size array_size_mask"
  and "init_safe\<^sub>\<T> (hd \<pi>\<^sub>4) input array1 array2 temp array1_size array_size_mask"
shows "\<pi>\<^sub>3 \<approx>\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4"
  (* First induct \<approx>\<^sub>\<L>\<^sub>\<pi> (TODO move to reusable rule)*)
  using assms apply (simp add: tpod_conformant_def)
  apply (simp add: conformant4_def)
  apply (induct "\<pi>\<^sub>3", simp_all)
  apply (induct "\<pi>\<^sub>4", simp_all)
  apply (elim conjE)

  apply (simp add: low_equiv_s_def)

  apply (simp add: init_safe\<^sub>\<T>_def ex01_\<Pi>_safe_def)

  (* Probably remove any non needed assms *)
  apply (auto)

  (* Clear any non speculative and non trusted states *)
  apply (simp add: low_equiv_s_def)
  apply (auto)


end
theory Ex05
  imports ExBase "../TPOD"
begin 

(*TODO look an unoptimized code to see about for loop *)

definition "ex05_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64],
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 4,
   3 \<mapsto> goto XX,
   
   4 \<mapsto> RAX := mem[Reg RDI, el]:u64,
   5 \<mapsto> RAX := RAX - 1,
   6 \<mapsto> mem := mem with [Reg RDI + 1, el]:u64 <- RAX,
   7 \<mapsto> goto 8,

   8 \<mapsto> RAX := mem[Const array2_addr],
   9 \<mapsto> RDX := mem[Const array1_addr],
  10 \<mapsto> RCX := (Reg RDI),
  11 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
  12 \<mapsto> v1 := Reg RCX,
  13 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v282),
  14 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  15 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const (12),
  16 \<mapsto> v287 := (Reg RDX),
  17 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v287)),
  18 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  19 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  20 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  21 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  22 \<mapsto> v1 := mem[Reg RDI + 1, el]:u64
  22 \<mapsto> v1 := v1 - 1
  22 \<mapsto> mem := mem with [Reg RDI + 1, el]:u64 <- mem[Reg RDI + 1, el]:u64 - 1, read decrement
  23 \<mapsto> CF := (BinOp (Reg RDI + 1) (asm_lte) (Reg RAX)),
  24 \<mapsto> when (Reg CF) goto XX,
  25 \<mapsto> mem := mem with [Reg RDI + 1, el]:u64 <- mem[Reg RDI + 1, el]:u64 - 1, write
  26 \<mapsto> goto 8,

  XX \<mapsto> goto HALT
]"

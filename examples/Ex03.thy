theory Ex02
  imports ExBase "../TPOD"
begin 

   0 \<mapsto> RAX := mem[Const array1_size_addr],
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,

   4 \<mapsto> RAX := mem[Const array2_addr],
   5 \<mapsto> RDX := mem[Const array1_addr],
   6 \<mapsto> RCX := (Reg RDI),
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> v274 := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v274)),
  10 \<mapsto> RDX := mem[Reg RDX],
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> v284 := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v284)),
  14 \<mapsto> RDX := mem[Reg RAX],
  15 \<mapsto> RAX := mem[Const temp_addr],
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr] <- (Reg RAX),
  18 \<mapsto> goto 18,

  19 \<mapsto> goto HALT

   0 \<mapsto> RAX := mem[Const array1_size_addr],
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 3,
   3 \<mapsto> goto 18,

   4 \<mapsto> RAX := mem[Const array1_addr],
   5 \<mapsto> RDX := (Reg RDI),
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> #303 := (Reg RDX)
   9 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg #303))
  10 \<mapsto> RAX := mem[Reg RAX]
  11 \<mapsto> RDI := RAX
  12 \<mapsto> RSP := RSP - 8
00001b3e: mem := mem with [RSP, el]:u64 <- 0x1245
00001b41: call @leakByteNoinlineFunction with return %000005e4

00001e0f: sub leakByteNoinlineFunction()
000004b5: 
000004bc: #54 := RBP
000004bf: RSP := RSP - 8
000004c2: mem := mem with [RSP, el]:u64 <- #54
000004c9: RBP := RSP
000004d0: mem := mem with [RBP - 8, el]:u64 <- RDI
000004d7: RAX := mem[0x4040, el]:u64
000004de: RDX := mem[RBP - 8, el]:u64
000004ef: RDX := RDX << 0xC
00000513: #58 := RDX
00000516: RAX := RAX + #58
0000052f: RDX := mem[RAX, el]:u64
00000536: RAX := mem[0x4058, el]:u64
00000543: RAX := RAX & RDX
0000055c: mem := mem with [0x4058, el]:u64 <- RAX
00000567: RBP := mem[RSP, el]:u64
0000056a: RSP := RSP + 8
00000573: #61 := mem[RSP, el]:u64
00000576: RSP := RSP + 8
0000057a: call #61 with noreturn
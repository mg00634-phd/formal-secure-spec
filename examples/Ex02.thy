theory Ex02
  imports ExBase "../TPOD"
begin 

definition "ex02_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_size_addr],
   1 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   2 \<mapsto> when (Reg CF) goto 2,
   3 \<mapsto> goto 1,

   1 \<mapsto> RAX := mem[Const array2_addr],
   4 \<mapsto> RDX := mem[Const array1_addr],
   5 \<mapsto> RCX := (Reg RDI),
   6 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   7 \<mapsto> v274 := (Reg RCX),
   
   8 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
   9 \<mapsto> v284 := (Reg RDX),
  10 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v284)),
  11 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  12 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  13 \<mapsto> RAX := (BinOp (Reg RAX) (&) (Reg RDX)),
  14 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  15 \<mapsto> goto 3,

   3 \<mapsto> goto HALT
]"

00001b43: 
00001b48: RAX := mem[Const array1_addr, el]:u64
00001b4f: RDX := mem[RBP - 8, el]:u64
00001b60: RDX := RDX << 3
00001b84: #308 := RDX
00001b87: RAX := RAX + #308
00001ba0: RAX := mem[RAX, el]:u64
00001ba7: RDI := RAX
00001bb0: RSP := RSP - 8
00001bb3: mem := mem with [RSP, el]:u64 <- 0x11DB
00000359: 
0000037b: RAX := mem[Const array2_addr, el]:u64
00000382: RDX := RDI



end

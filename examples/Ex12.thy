theory Ex12
  imports ExBase "../TPOD"
begin 


(*TODO look an unoptimized code, needs RDI and RSI*)

definition "ex12_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> mem := mem with [RBP - 8, el]:u64 <- (Reg RDI)
   0 \<mapsto> mem := mem with [RBP - 16, el]:u64 <- (Reg RSI)
   0 \<mapsto> RDX := (Reg RDI)
   0 \<mapsto> RAX := (Reg RSI)
   0 \<mapsto> v1 := (Reg RAX)
   0 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v1))
   0 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   0 \<mapsto> CF := (BinOp (Reg RDX) (asm_lt) (Reg RAX)),
   0 \<mapsto> when (Reg CF) goto 1,
   0 \<mapsto> goto 2,

   4 \<mapsto> RAX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RDX := mem[Const array1_addr, el]:u64,
   1 \<mapsto> RSI := mem[(BinOp (Reg RBP) (-) (Const 8)), el]:u64
   1 \<mapsto> RCX := mem[(BinOp (Reg RBP) (-) (Const 16)), el]:u64
   1 \<mapsto> v1 := RSI
   0 \<mapsto> RCX := (BinOp (Reg RCX) (+) (Reg v1))
   7 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
   8 \<mapsto> v2 := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v2)),
  10 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> v3 := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v3)),
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),
  18 \<mapsto> goto 18,

   3 \<mapsto> goto HALT
"]
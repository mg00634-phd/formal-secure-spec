theory Ex15
  imports ExBase "../TPOD"
begin 


definition "ex15_\<Delta>_vulnerable \<equiv> [
   0 \<mapsto> RAX := mem[Const array1_addr, el]:u64,
   1 \<mapsto> when (BinOp (Reg RDI) (asm_lt) (Reg RAX)) goto 3,
   2 \<mapsto> goto 1,

   1 \<mapsto> RDX := mem[Const array2_addr, el]:u64,
   4 \<mapsto> RCX := mem[Const array1_addr, el]:u64,
   5 \<mapsto> RAX := (Reg RDI),
   1 \<mapsto> RAX := mem[Reg RAX, el]:u64,
   6 \<mapsto> RAX := (BinOp (Reg RAX) (<<) (Const 3)),
   7 \<mapsto> v274 := (Reg RCX),
  10 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v284)),
   1 \<mapsto> RAX := mem[Reg RAX, el]:u64,
   8 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
   9 \<mapsto> v284 := (Reg RDX),
  10 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v284)),
  11 \<mapsto> RDX := mem[Reg RAX, el]:u64
  12 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  13 \<mapsto> RAX := (BinOp (Reg RAX) (&) (Reg RDX)),
  14 \<mapsto> mem := mem with [Const temp_addr] <- (Reg RAX),
  15 \<mapsto> goto 2,

   2 \<mapsto> goto HALT
]"

end
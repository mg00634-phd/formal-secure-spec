theory Ex08
  imports ExBase "../TPOD"
begin 

definition "ex08_\<Delta>_vulnerable \<equiv> [
   4 \<mapsto> RDX := mem[Const array2_addr, el]:u64,
   5 \<mapsto> RCX := mem[Const array1_addr, el]:u64,
   6 \<mapsto> RAX := mem[Const array1_size_addr, el]:u64,
   7 \<mapsto> CF := (BinOp (Reg RDI) (asm_lt) (Reg RAX)),
   8 \<mapsto> when (Reg CF) goto 12,
   9 \<mapsto> goto 10,

  10 \<mapsto> RAX := (Const 0),
  11 \<mapsto> goto 222,

  12 \<mapsto> RAX := mem[Reg RDI, el]:u64,
   7 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Const 1)),
   7 \<mapsto> RAX := (BinOp (Reg RAX) (<<) (Const 3)),
   7 \<mapsto> goto 222,

 222 \<mapsto> v1 := (Reg RCX),
   9 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v1)),
  10 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  11 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  12 \<mapsto> v2 := (Reg RDX),
  13 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v2)),
  14 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  15 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  16 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  17 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),

  24 \<mapsto> goto HALT
]"

end

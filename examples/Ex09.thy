theory Ex09
  imports ExBase "../TPOD"
begin 

(*requires RDI and RSI RSI is a pointer *)
definition "ex09_\<Delta>_vulnerable \<equiv> [
   1 \<mapsto> RAX := (Reg RSI),
   2 \<mapsto> RAX := mem[Reg RAX, el]:u64,
   3 \<mapsto> v1 := (Reg RAX),
   4 \<mapsto> ZF := (BinOp (Const 0) (asm_eq) (Reg v1)),
   5 \<mapsto> when ZF goto 21,
   6 \<mapsto> goto 7,

   7 \<mapsto> RAX := mem[Const array2_addr],
   8 \<mapsto> RDX := mem[Const array1_addr],
   9 \<mapsto> RCX := (Reg RDI),
  10 \<mapsto> RCX := (BinOp (Reg RCX) (<<) (Const 3)),
  11 \<mapsto> v1 := (Reg RCX),
  12 \<mapsto> RDX := (BinOp (Reg RDX) (+) (Reg v1)),
  13 \<mapsto> RDX := mem[Reg RDX, el]:u64,
  14 \<mapsto> RDX := (BinOp (Reg RDX) (<<) (Const 12)),
  15 \<mapsto> v2 := (Reg RDX),
  16 \<mapsto> RAX := (BinOp (Reg RAX) (+) (Reg v2)),
  17 \<mapsto> RDX := mem[Reg RAX, el]:u64,
  18 \<mapsto> RAX := mem[Const temp_addr, el]:u64,
  19 \<mapsto> RAX := (BinOp (Reg RAX) (asm_and) (Reg RDX)),
  20 \<mapsto> mem := mem with [Const temp_addr, el]:u64 <- (Reg RAX),

  21 \<mapsto> goto HALT
]"

end
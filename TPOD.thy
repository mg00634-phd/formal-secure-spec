theory TPOD
    imports Main AdversaryModel
begin

section \<open>Formalization of the Security Property\<close>

definition "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<equiv>
  conformant4 (map MachineState.truncate \<pi>\<^sub>1) (map MachineState.truncate \<pi>\<^sub>2) (map MachineState.truncate \<pi>\<^sub>3) (map MachineState.truncate \<pi>\<^sub>4) \<and>
  conformant \<pi>\<^sub>1 \<and>
  conformant \<pi>\<^sub>2 \<and>
  conformant \<pi>\<^sub>3 \<and>
  conformant \<pi>\<^sub>4 \<and>
  (\<forall>s\<^sub>i. s\<^sub>i \<in> set \<pi>\<^sub>1 \<longrightarrow> \<not>mispred (n s\<^sub>i) (\<beta> s\<^sub>i) (pc s\<^sub>i)) \<and>
  (\<forall>s\<^sub>i. s\<^sub>i \<in> set \<pi>\<^sub>2 \<longrightarrow> \<not>mispred (n s\<^sub>i) (\<beta> s\<^sub>i) (pc s\<^sub>i)) \<and>
  (\<exists>s\<^sub>i. s\<^sub>i \<in> set \<pi>\<^sub>3 \<longrightarrow> mispred (n s\<^sub>i) (\<beta> s\<^sub>i) (pc s\<^sub>i)) \<and>
  (\<exists>s\<^sub>i. s\<^sub>i \<in> set \<pi>\<^sub>4 \<longrightarrow> mispred (n s\<^sub>i) (\<beta> s\<^sub>i) (pc s\<^sub>i)) \<and>
  (op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>1 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>2 \<and> op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>1 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>3 \<and> op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>1 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4
        \<and> op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>2 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>3 \<and> op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>2 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4
        \<and> op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>3 = op\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>4) \<and>
  (op\<^sub>\<H>\<^sub>\<pi> \<pi>\<^sub>1 = op\<^sub>\<H>\<^sub>\<pi> \<pi>\<^sub>3 \<and> op\<^sub>\<H>\<^sub>\<pi> \<pi>\<^sub>2 = op\<^sub>\<H>\<^sub>\<pi> \<pi>\<^sub>4) \<and>
  (\<pi>\<^sub>1 \<approx>\<^sub>\<L>\<^sub>\<pi> \<pi>\<^sub>2 \<and> ((hd \<pi>\<^sub>3) \<approx>\<^sub>\<L>\<^sub>s (hd \<pi>\<^sub>4)))
"


text \<open>There should be no valid state transition from the last state of a conformant halt trace.\<close>

lemma last_s_no_trans: 
  assumes "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4"
  and "s\<^sub>1 = last (map MachineState.truncate \<pi>\<^sub>1)"
  and "s\<^sub>2 = last (map MachineState.truncate \<pi>\<^sub>2)"
  and "s\<^sub>3 = last (map MachineState.truncate \<pi>\<^sub>3)"
  and "s\<^sub>4 = last (map MachineState.truncate \<pi>\<^sub>4)"
shows "\<not>(s\<^sub>1 \<leadsto> s\<^sub>1') \<and> \<not>(s\<^sub>2 \<leadsto> s\<^sub>2') \<and> \<not>(s\<^sub>3 \<leadsto> s\<^sub>3') \<and> \<not>(s\<^sub>4 \<leadsto> s\<^sub>4')"
  apply (intro conjI)
  using assms apply (simp_all add: tpod_conformant_def conformant_def Let_def)
  using last_trace_no_trans by auto

text \<open>If there is no valid transition then we must be stuttering.\<close>

lemma [simp]: "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<Longrightarrow>   
  length \<pi>\<^sub>1 = length \<pi>\<^sub>2 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>3 = length \<pi>\<^sub>4"
  by (simp_all add: tpod_conformant_def conformant4_def) 

text \<open>If there is no valid transition then we must be stuttering.\<close>

lemma [simp]: "tpod_conformant \<pi>\<^sub>1 \<pi>\<^sub>2 \<pi>\<^sub>3 \<pi>\<^sub>4 \<Longrightarrow>   
  length \<pi>\<^sub>1 = length \<pi>\<^sub>2 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>1 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>3 \<and>
  length \<pi>\<^sub>2 = length \<pi>\<^sub>4 \<and>
  length \<pi>\<^sub>3 = length \<pi>\<^sub>4"
  by (simp_all add: tpod_conformant_def conformant4_def) 


end

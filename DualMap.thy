theory DualMap
  imports Main
begin

section \<open>dual_map a dual indexed map in the form. This is implemented as a map to a map to avoid 
the infinite set issues with the maps domain.\<close>

type_synonym ('a, 'b, 'c) "dual_map" = "'a \<rightharpoonup> 'b \<rightharpoonup> 'c"

text \<open>Get the value v in the map at k1 k2.\<close>

definition dual_get :: "('k\<^sub>1, 'k\<^sub>2, 'v) dual_map \<Rightarrow> 'k\<^sub>1 \<Rightarrow> 'k\<^sub>2 \<Rightarrow> 'v option" ("_ [_, _]\<^sub>m") where
"dual_get m k\<^sub>1 k\<^sub>2 \<equiv> let m\<^sub>s = (m k\<^sub>1) in if m\<^sub>s = None then None else the m\<^sub>s k\<^sub>2"

(* If k\<^sub>1 doesn't exist then return None *)
lemma [simp]: "\<lbrakk>m\<^sub>1 k\<^sub>1 = None\<rbrakk> \<Longrightarrow> m\<^sub>1 [k\<^sub>1, k\<^sub>2]\<^sub>m = None"
  by (simp add: dual_get_def)

(* If k\<^sub>1 does exist then return m\<^sub>2 k\<^sub>2 where m\<^sub>1 k\<^sub>1 = Some m\<^sub>2 *)
lemma [simp]: "\<lbrakk>m\<^sub>1 k\<^sub>1 = Some m\<^sub>2\<rbrakk> \<Longrightarrow> m\<^sub>1 [k\<^sub>1, k\<^sub>2]\<^sub>m = m\<^sub>2 k\<^sub>2"
  by (simp add: dual_get_def)


text \<open>Put the value v in the map at k1 k2.\<close>

definition dual_put :: "('k\<^sub>1, 'k\<^sub>2, 'v) dual_map \<Rightarrow> 'k\<^sub>1 \<Rightarrow> 'k\<^sub>2 \<Rightarrow> 'v \<Rightarrow> ('k\<^sub>1, 'k\<^sub>2, 'v) dual_map" 
    ("_ [_, _]\<^sub>m \<mapsto> _") where
"(m [k\<^sub>1, k\<^sub>2]\<^sub>m \<mapsto> v) = (
    let ms = (m k\<^sub>1) in 
      if ms = None then m(k\<^sub>1 \<mapsto> [k\<^sub>2 \<mapsto> v])
      else m(k\<^sub>1 \<mapsto> ((the ms)(k\<^sub>2 \<mapsto> v)))
)
"

(* Setting the value at k\<^sub>1, k\<^sub>2 to v succeeds *)
lemma [simp]: "(m [k\<^sub>1, k\<^sub>2]\<^sub>m \<mapsto> v)[k\<^sub>1, k\<^sub>2]\<^sub>m = Some v"
  by (simp add: dual_put_def Let_def)

(* Maps at all k other than k\<^sub>1 do not change *)
lemma [simp]: "k\<^sub>1 \<noteq> k \<Longrightarrow> (m [k\<^sub>1, k\<^sub>2]\<^sub>m \<mapsto> v) k = m k"
  by (simp add: dual_put_def Let_def)

(* Maps at all k in map k\<^sub>1 other than k\<^sub>2 do not change *)
lemma [simp]: "k\<^sub>2 \<noteq> k \<Longrightarrow> (m [k\<^sub>1, k\<^sub>2]\<^sub>m \<mapsto> v)[k\<^sub>1, k]\<^sub>m = (m [k\<^sub>1, k]\<^sub>m)"
  by (auto simp add: dual_put_def Let_def)

(* Given that m n' is defined, updating via dual_put will remain defined *)
lemma [simp]: "m n' \<noteq> None \<Longrightarrow> (m [a, b]\<^sub>m \<mapsto> c) n' \<noteq> None"
  by (simp add: dual_put_def Let_def)


text \<open>Copy the data at k1 in m to k1'\<close>

definition dual_copy :: "('k\<^sub>1, 'k\<^sub>2, 'v) dual_map \<Rightarrow> 'k\<^sub>1 \<Rightarrow> 'k\<^sub>1 \<Rightarrow> ('k\<^sub>1, 'k\<^sub>2, 'v) dual_map" where
"dual_copy m k\<^sub>1 k\<^sub>1' = (let ms = (m k\<^sub>1) in (
  if (ms = None)
  then m (k\<^sub>1' := None)
  else m (k\<^sub>1' \<mapsto> (the (ms)))
))"

(* Only data in k\<^sub>1' will change *)
lemma [simp]: "k \<noteq> k\<^sub>1' \<Longrightarrow> (dual_copy m k\<^sub>1 k\<^sub>1') k = m k"
  by (smt dual_copy_def fun_upd_other)

(* results in k\<^sub>1 equal results in k\<^sub>1 *)
lemma [simp]: "(dual_copy m k\<^sub>1 k\<^sub>1') k\<^sub>1' = m k\<^sub>1"
  by (simp add: Let_def dual_copy_def)

(* results in k\<^sub>1 equal results in k\<^sub>1 *)
lemma [simp]: "m k\<^sub>1 \<noteq> None \<Longrightarrow> (dual_copy m k\<^sub>1 k\<^sub>1') = m (k\<^sub>1' \<mapsto> the (m k\<^sub>1))"
  by (simp add: Let_def dual_copy_def)

end